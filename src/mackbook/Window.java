package mackbook;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.MalformedURLException;
import java.net.Socket;

import java.util.ArrayList;
import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <pre>
 * Authors: Jared Rathbun and Alexander Royer
 * Course: CSC 2620
 * Due Date: 11/10/2020
 *
 * This class creates a GUI (window) for the user to interact with.
 * </pre>
 */
public class Window extends JFrame implements Runnable
{

    private String name, IP;
    private int port;
    private Socket connectionSocket;
    private Server server;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private JPanel timelinePanel, newPostPanel, searchPanel, cards,
            nameSearchPanel, hashtagSearchPanel, substringSearchPanel;
    private JButton viewTimelineButton;
    private JScrollPane searchResultPanel;
    private boolean isNewPostPanelVisible, isTimelineVisible,
            isNameSearchResultVisible, isHashtagSearchResultVisible, 
            isSubstringSearchResultVisible;
    private static GridBagConstraints gbc;
    private ArrayList<Post> timeline;
    
    private final String TIMELINE_ID = "Timeline Panel";
    private final String NEW_POST_ID = "New Post Panel";
    private final String SEARCH_ID = "Search Panel";
    private final String NAME_SEARCH_ID = "Name Search Panel";
    private final String HASHTAG_SEARCH_ID = "Hashtag Search Panel";
    private final String SUBSTRING_SEARCH_ID = "Substring Search Panel";

    /**
     * Constructor which sets the name, IP Address, Port, and the Server upon
     * initialization.
     * 
     * @param name The name of the person.
     * @param IP The IP Address.
     * @param port The port.
     * @param server The Server object.
     */
    public Window(String name, String IP, int port, Server server)
    {
        // Super call to JFrame, sets the title of the frame.
        super("MackBook - " + name);

        // Setters.
        this.name = name;
        this.IP = IP;
        this.port = port;
        this.server = server;
        setSize(720, 600);
        setResizable(true);

        // Set a Gradient background using Merrimack's colors.
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;
                final int width = getWidth();
                final int height = getHeight();
                g2d.setPaint(new GradientPaint(0, 0, new Color(94, 173, 203),
                        width, height, new Color(6, 52, 91), true));
                g2d.fillRect(0, 0, width, height);
            }
        };
        setContentPane(panel);

        // Initializers.
        gbc = new GridBagConstraints();
        cards = new JPanel(new CardLayout());
        cards.setOpaque(false);
        timeline = new ArrayList<>();

        // Build the GUI.
        buildGUI();

        add(cards);
    }

    /**
     * Run method to connect to create a Socket and uses I/O object stream.
     */
    @Override
    public void run()
    {
        try
        {
            // Create the new socket object and Obejct O/I stream.
            connectionSocket = new Socket(IP, port);
            outputStream = new ObjectOutputStream(connectionSocket
                    .getOutputStream());
            inputStream = new ObjectInputStream(connectionSocket
                    .getInputStream());
        } catch (IOException e)
        {
            System.err.println("Cannot start connection at: " + IP + ":"
                    + port + "\n" + e.getMessage());
        }

        while (true)
        {
            try
            {
                // Create a Post object to add to the timeline.
                Post newPost = (Post) inputStream.readObject();
                server.addNewPost(newPost);
                buildTimelinePanel();
                validate();
            } catch (IOException | ClassNotFoundException ex)
            {
                System.err.println("Unable to read object from InputStream.");
            }
        }
    }

    /**
     * Build the GUI for the user to use.
     */
    private void buildGUI()
    {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);

        setFrameIcon();
        buildHeader();
        buildNewPostPanel();
        buildTimelinePanel();
        buildSearchPanel();
    }

    /**
     * Set the frame icon to the iconic MackBook® logo.
     */
    private void setFrameIcon()
    {
        ImageIcon icon = new ImageIcon("mProfilePictureTransparent.png");
        setIconImage(icon.getImage());
    }

    /**
     * Builds the MackBook header JPanel that uses GBC.
     */
    private void buildHeader()
    {
        // Creates a headerPanel that uses GBC
        JPanel headerPanel = new JPanel(new GridBagLayout());
        headerPanel.setOpaque(false);
        BufferedImage headerImg = null;

        // Tries to load the Mackbook® header image.
        try
        {
            headerImg = ImageIO.read(new File("headerTransparent.png"));
        } catch (IOException ex)
        {
            System.err.println("Error reading image: IOException");
        }

        gbc.gridx = 0;
        gbc.gridy = 0;
        headerPanel.add(new JLabel(new ImageIcon(headerImg)), gbc);
        // Add the Mackbook® header image to the header panel.
        
        // Create a new JPanel called buttonsPanel
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setOpaque(false);

        // Creates a JButton that allows the user to add a new post to timeline.
        JButton newPostButton = new JButton("New Post");
        newPostButton.addActionListener((ActionEvent e) -> {
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, NEW_POST_ID);
        });
        buttonsPanel.add(newPostButton);
        // Adds the new post button to the buttons panel.
        
        // Creates a JButton that allows the user to search the button
        JButton searchButton = new JButton("Search Timeline");
        searchButton.addActionListener((ActionEvent e) -> {
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, SEARCH_ID);
        });
        buttonsPanel.add(searchButton);
        // Adds the search button the the buttons panel.
        
        // Creates a new JButton that views the timeline.
        viewTimelineButton = new JButton("View Timeline");
        viewTimelineButton.addActionListener((ActionEvent e) -> {
            buildTimelinePanel();
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, TIMELINE_ID);
        });
        buttonsPanel.add(viewTimelineButton);
        // Adds the view timeline button to the buttons panel.
        
        // Creates a JButtonn that saves the current timeline.
        JButton saveButton = new JButton("Save Timeline");
        saveButton.addActionListener((ActionEvent e) -> {
            server.saveTimeline();
        });
        buttonsPanel.add(saveButton);
        // Adds the save button to the buttons panel.
        
        // Creates a JButton that exits the program.
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        buttonsPanel.add(exitButton);
        // Add the exit button the the buttons panel.
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        headerPanel.add(buttonsPanel, gbc);
        // Add the buttons panel to the header panel.

        // Adds the header panel to the north side.
        add(headerPanel, BorderLayout.NORTH);
        
        // Refresh GUI.
        validate();
    }

    /**
     * Builds a new JPanel called newPostPanel that creates a Panel for the user
     * to add a post to the timeline.
     */
    private void buildNewPostPanel()
    {
        // Reset the newPostPanel if it is true.
        if (isNewPostPanelVisible)
        {
            remove(newPostPanel);
            newPostPanel = null;
        }

        // Create a new JPanel that uses GBC.
        newPostPanel = new JPanel(new GridBagLayout());
        newPostPanel.setOpaque(false);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(5, 5, 5, 5);
        JLabel headerLabel = new JLabel("Create a New Post");
        headerLabel.setForeground(Color.WHITE);
        headerLabel.setFont(new Font("Arial", Font.BOLD, 20));
        newPostPanel.add(headerLabel, gbc);
        // Add the headerLabel to the newPostPanel.

        // Object declarations.
        JFileChooser imgChooser = new JFileChooser();
        imgChooser.setFileFilter(new FileNameExtensionFilter("Images",
                new String[] { "jpg", "jpeg", "png", "gif" }));

        // Create a new JPanel for pictures and text that uses GBC.
        JPanel picAndTextPanel = new JPanel(new GridBagLayout());
        picAndTextPanel.setOpaque(false);
        
        // Create a new JTextField for text
        JTextArea textField = new JTextArea("Enter your message...", 5, 40);
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e)
            {
                textField.setText("");
            }
        });
        textField.setLineWrap(true);
        
        // Create a new JScrollPane that passes in the textField.
        JScrollPane scrollPane = new JScrollPane(textField);
        gbc.gridx = 1;
        gbc.gridy = 0;
        picAndTextPanel.add(scrollPane, gbc);
        // Add the scrollPane to the picAndTextPanel.
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.BASELINE;
        newPostPanel.add(picAndTextPanel, gbc);
        // Add the picAndTextPanel to the newPostPanel.

        // Create a panel for a link and button to choose an image.
        JPanel linkAndButtonPanel = new JPanel(new GridBagLayout());
        linkAndButtonPanel.setOpaque(false);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        JLabel linkLabel = new JLabel("Link:   ");
        linkLabel.setOpaque(false);
        linkLabel.setForeground(Color.WHITE);
        linkAndButtonPanel.add(linkLabel, gbc);
        // Add the linkLabel to the linkAndButtonPanel.

        // Create a JTextField that is used to prompt for a link to post.
        JTextField linkField = new JTextField("Enter a link...", 30);
        linkField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e)
            {
                linkField.setText("");
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 0;
        linkAndButtonPanel.add(linkField, gbc);
        // Add the linkField to the linkAndButtonPanel.
        
        final JPanel[] fileAndButtonPanel = new JPanel[1];
        final File[] selectedFile = new File[1];
        JLabel[] fileLabel = new JLabel[1];
        JButton[] removeButton = new JButton[1];
        JButton imageButton = new JButton("Attach Image");
        imageButton.addActionListener((ActionEvent e) -> {
            if (imgChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
            {
                selectedFile[0] = imgChooser.getSelectedFile();
                fileAndButtonPanel[0] = new JPanel();
                fileAndButtonPanel[0].setOpaque(false);
                fileLabel[0] = new JLabel("Selected Image: "
                        + selectedFile[0].getName());
                fileLabel[0].setOpaque(false);
                fileLabel[0].setForeground(Color.RED);
                fileAndButtonPanel[0].add(fileLabel[0]);

                removeButton[0] = new JButton("Remove Image");
                removeButton[0].addActionListener((ActionEvent ea) -> {
                    selectedFile[0] = null;
                    fileLabel[0].setText("No file selected.");
                    validate();
                });
                fileAndButtonPanel[0].add(removeButton[0]);

                gbc.gridx = 0;
                gbc.gridy = 3;
                gbc.anchor = GridBagConstraints.LINE_START;
                gbc.insets = new Insets(5, 5, 5, 5);
                newPostPanel.add(fileAndButtonPanel[0], gbc);
                // Refresh the GUI.
                validate();
            }
        });
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.insets = new Insets(5, 5, 5, 5);
        linkAndButtonPanel.add(imageButton, gbc);
        // Add the imageButton to the linkAndButtonPanel.
        
        gbc.gridx = 0;
        gbc.gridy = 2;
        newPostPanel.add(linkAndButtonPanel, gbc);
        // Add the linkAndButtonPanel to the newPostPanel.
        
        // Create the new post JButton.
        JButton createButton = new JButton("Create New Post");
        createButton.addActionListener((ActionEvent e) -> {
            // Checks to see if one of the user fields are filled.
            final boolean isTextFilled = !(textField.getText().equals("")
                    || textField.getText().equals("Enter your message..."));
            final boolean isURLFilled = !(linkField.getText().equals("")
                    || linkField.getText().equals("Enter a link..."));
            final boolean isImgSelected = (selectedFile[0] != null);

            try
            {
                String text = textField.getText();
                String url = linkField.getText();

                if (!isTextFilled && !isURLFilled && !isImgSelected)
                {
                    /* If none of the fields have something in them, prompt and 
                    return to avoid an exception. */
                    JOptionPane.showMessageDialog(this,
                            "Please fill in at least one field.");
                    return;
                } else if (isTextFilled && isURLFilled && isImgSelected)
                {
                    outputStream.writeObject(new Post(name, text, url,
                            selectedFile[0]));
                } else if (isTextFilled && isURLFilled && !isImgSelected)
                {
                    outputStream.writeObject(new Post(name, text, url));
                } else if (isTextFilled && isImgSelected && !isURLFilled)
                {
                    outputStream.writeObject(new Post(name, text,
                            selectedFile[0]));
                } else if (isTextFilled && !isImgSelected && !isURLFilled)
                {
                    outputStream.writeObject(new Post(name, text));
                } else if (isURLFilled && !isTextFilled && !isImgSelected)
                {
                    outputStream.writeObject(new Post(name, url, true));
                } else if (isImgSelected && !isTextFilled && !isURLFilled)
                {
                    outputStream.writeObject(new Post(name, selectedFile[0]));
                }
            } catch (MalformedURLException ex)
            {
                JOptionPane.showMessageDialog(this, "Unable to create new "
                        + "post. \nReason: Invalid URL");
            } catch (IOException ex)
            {
                // Swallow Exception.
            }

            // Rebuild the timeline panel.
            buildTimelinePanel();

            // Clear the fields for the next creation.
            textField.setText("Enter your message...");
            linkField.setText("Enter a link...");
            if (fileLabel[0] != null)
            {
                fileLabel[0].setText("");
            }
            selectedFile[0] = null;
            if (fileAndButtonPanel[0] != null)
            {
                fileAndButtonPanel[0].remove(removeButton[0]);
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 3;
        newPostPanel.add(createButton, gbc);

        // Add the newPostPanel to the CardLayout.
        cards.add(newPostPanel, NEW_POST_ID);

        // Sets the postPanel to visible after creating it.
        isNewPostPanelVisible = true;
    }

    /**
     * Builds the timelinePanel that holds all of the users posts.
     */
    private void buildTimelinePanel()
    {
        // Resets the timeline panel if it is visible.
        if (isTimelineVisible)
        {
            remove(timelinePanel);
            timelinePanel = null;
        }
        
        // Create the timelinePanel object.
        timelinePanel = new JPanel(new GridBagLayout());
        timelinePanel.setOpaque(false);

        // Create the array of Posts and set it to the servers timeline.
        Post[] localTimeline = server.getTimeline();
        if (localTimeline.length > 0)
        {
            for (int i = 0; i < localTimeline.length; i++)
            {
                gbc.gridx = 0;
                gbc.gridy = i;
                timelinePanel.add(localTimeline[i], gbc);
            }

            // Create a JScrollPane.
            JScrollPane scrollPane = new JScrollPane(timelinePanel);
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane
                    .HORIZONTAL_SCROLLBAR_NEVER);
            cards.add(scrollPane, TIMELINE_ID);
            validate();
        } else
        {
            // Create a JPanel.
            JPanel emptyPanel = new JPanel();
            emptyPanel.setOpaque(false);
            JLabel emptyLabel = new JLabel("The timeline is empty :( \n "
                    + "Add some new posts to see the timeline come to life!");
            emptyLabel.setForeground(Color.WHITE);
            emptyPanel.add(emptyLabel);
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor = GridBagConstraints.CENTER;
            timelinePanel.add(emptyPanel, gbc);
            cards.add(timelinePanel, TIMELINE_ID);
        }
        isTimelineVisible = true;
    }

    /**
     * Builds the search JPanel that allows the users to search the timeline.
     */
    private void buildSearchPanel()
    {
        // A panel to hold all of the components for searching.
        searchPanel = new JPanel(new GridBagLayout());
        searchPanel.setOpaque(false);

        // Creates a JLabel that is the header label.
        JLabel headerLabel = new JLabel("Search the Timeline");
        headerLabel.setForeground(Color.WHITE);
        headerLabel.setFont(new Font("Arial", Font.BOLD, 20));
        gbc.gridx = 0;
        gbc.gridy = 0;
        searchPanel.add(headerLabel, gbc);
        // Add the headerLabel to the searchPanel.
        
        // Create a JPanel that uses GBL.
        JPanel optionsPanel = new JPanel(new GridBagLayout());
        optionsPanel.setOpaque(false);
        optionsPanel.setBorder(BorderFactory
                .createTitledBorder("What would you like to do?"));
        
        TitledBorder tb = (TitledBorder) optionsPanel.getBorder();
        tb.setTitleColor(Color.WHITE);

        // Create a JPanel that uses GBL.
        JPanel actionsPanel = new JPanel(new CardLayout());
        actionsPanel.setOpaque(false);
        
        // Creates three IDs to identify which search is being done.
        final String NAMES_ID = "Names";
        final String HASHTAG_ID = "Hashtag";
        final String SUBSTRING_ID = "Substring";

        // Create a text field for the name(s) to search for.
        JPanel namesPanel = new JPanel(new GridBagLayout());
        namesPanel.setOpaque(false);
        JTextField nameField = new JTextField(10);
        nameField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                nameField.setText("");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        JLabel nameLabel = new JLabel("Enter the name(s): ");
        nameLabel.setForeground(Color.WHITE);
        namesPanel.add(nameLabel, gbc);
        // Add a new JLabel to the namesPanel.
        
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        namesPanel.add(nameField, gbc);
        // Adds the nameField to the namesPanel.
        
        /* Add a button that checks the input and if it is valid searches the 
        timeline. */
        JButton searchByNameButton = new JButton("Search");
        searchByNameButton.addActionListener((ActionEvent e) -> {
            // Creates a CardLayout.
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, NAME_SEARCH_ID);
            buildNameSearchPanel(nameField);
            // Refresh GUI.
            if(isHashtagSearchResultVisible == true || 
                    isSubstringSearchResultVisible == true)
            {
                searchPanel.remove(nameSearchPanel);
                nameSearchPanel = null;
            }
            validate();
        });
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        namesPanel.add(searchByNameButton, gbc);
        // Add the searchByNameButton to the namesPanel.
        
        // Refresh the GUI.
        validate();

        // add the namesPanel to the ActionPanel.
        actionsPanel.add(namesPanel, NAMES_ID);

        // Create a new JPanel that uses GBL.
        JPanel hashtagPanel = new JPanel(new GridBagLayout());
        hashtagPanel.setOpaque(false);
        JTextField hashtagField = new JTextField(10);
        hashtagField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e)
            {
                hashtagField.setText("");
                hashtagField.removeFocusListener(this);
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        JLabel hashtagLabel = new JLabel("Enter a hashtag: ");
        hashtagLabel.setForeground(Color.WHITE);
        hashtagPanel.add(hashtagLabel, gbc);
        // Add a new JLabel to the hashtagPanel.
        
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        hashtagPanel.add(hashtagField, gbc);
        // Add the hashtagField to the hashtagPanel.

        // Creates a new JButton that searches the user specified hashtag.
        JButton searchByHashtagButton = new JButton("Search");
        searchByHashtagButton.addActionListener((ActionEvent e) -> {
            // Create a CardLayout.
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, HASHTAG_SEARCH_ID);
            buildHashtagSearchPanel(hashtagField);
            // Refresh the GUI.
            validate();
            if(isNameSearchResultVisible == true || 
                    isSubstringSearchResultVisible == true)
            {
                searchPanel.remove(hashtagSearchPanel);
                hashtagSearchPanel = null;
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        hashtagPanel.add(searchByHashtagButton, gbc);
        // Add the searchByHastagButton to the hashtagPanel.
        
        actionsPanel.add(hashtagPanel, HASHTAG_ID);
        // Add the hashtagPanel to the actionsPanel.
        
        // Create a new JPanel to the GBL.
        JPanel substringPanel = new JPanel(new GridBagLayout());
        substringPanel.setOpaque(false);
        JTextField substringField = new JTextField(10);
        substringField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                substringField.setText("");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        JLabel substringLabel = new JLabel("Enter a substring: ");
        substringLabel.setForeground(Color.WHITE);
        substringPanel.add(substringLabel, gbc);
        // Adds a new JLabel to the substringPanel.
        
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        substringPanel.add(substringField, gbc);
        // Adds the substringField to the substringPanel.
        
        // Create a new JButton that searches by the user specified substring.
        JButton searchBySubstringButton = new JButton("Search");
        searchBySubstringButton.addActionListener((ActionEvent e) -> {
            // Create a CardLayout.
            CardLayout cL = (CardLayout) (cards.getLayout());
            cL.show(cards, SUBSTRING_SEARCH_ID);
            buildSubstringSearchPanel(substringField);
            
            if(isNameSearchResultVisible == true || 
                    isHashtagSearchResultVisible == true)
            {
                searchPanel.remove(hashtagSearchPanel);
                substringSearchPanel = null;
            }

            validate();
        });
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        substringPanel.add(searchBySubstringButton, gbc);
        actionsPanel.add(substringPanel, SUBSTRING_ID);
        // Add the substringPanel to the actionsPanel.
        
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        optionsPanel.add(actionsPanel, gbc);
        // Add the actionsPanel to the optionsPanel.
        
        // Create a new ButtonGroup.
        ButtonGroup buttonGroup = new ButtonGroup();
        
        // Creates a new JRadioButton that is used to search names.
        JRadioButton nameButton = new JRadioButton("Search by Name(s)");
        nameButton.setOpaque(false);
        nameButton.setForeground(Color.WHITE);
        nameButton.addActionListener((ActionEvent e) -> {
            // Create the CardLayout.
            CardLayout cL = (CardLayout) actionsPanel.getLayout();
            cL.show(actionsPanel, NAMES_ID);
            
            if (searchResultPanel != null)
                searchPanel.remove(searchResultPanel);
            // Refresh the GUI.
            validate();
        });
        nameButton.setSelected(true);
        buttonGroup.add(nameButton);
        optionsPanel.add(nameButton);
        // Add the nameButton to the buttonGroup then the optionsPanel.
        
        // Creates a new JRadioButton that is used to search by hashtag.
        JRadioButton hashtagButton = new JRadioButton("Search by hashtag");
        hashtagButton.setOpaque(false);
        hashtagButton.setForeground(Color.WHITE);
        hashtagButton.addActionListener((ActionEvent e) -> {
            // Creates a CardLayout.
            CardLayout cL = (CardLayout) actionsPanel.getLayout();
            cL.show(actionsPanel, HASHTAG_ID);
            // Refresh GUI.
            if (searchResultPanel != null)
                remove(searchResultPanel);
            validate();
        });
        buttonGroup.add(hashtagButton);
        optionsPanel.add(hashtagButton);
        // Add the hashtagButton to the buttonGroup then the optionsPanel.
        
        // Creates a new JRadioButton that is used to search by a substring.
        JRadioButton substringButton = new JRadioButton("Search by substring");
        substringButton.setOpaque(false);
        substringButton.setForeground(Color.WHITE);
        substringButton.addActionListener((ActionEvent e) -> {
            // Create a CardLayout.
            CardLayout cL = (CardLayout) actionsPanel.getLayout();
            cL.show(actionsPanel, SUBSTRING_ID);
            // Refresh GUI.
            validate(); 
        });
        buttonGroup.add(substringButton);
        optionsPanel.add(substringButton);
        // Add the substringButton to the buttonGroup and the optionsPanel.
        
        searchPanel.revalidate();
        gbc.gridx = 0;
        gbc.gridy = 1;
        searchPanel.add(optionsPanel, gbc);
        cards.add(searchPanel, SEARCH_ID);
        // Add the searchPanel to the cards.
        
        // Refresh the GUI.
        validate();
    }

    /**
     * Builds the nameSearch panel that shows the users search results by name.
     * @param nameField The user of the posts the user is searching for.
     */
    private void buildNameSearchPanel(JTextField nameField)
    {
        // Create a CardLayout.
        CardLayout cL = (CardLayout) (cards.getLayout());
        cL.show(cards, NAME_SEARCH_ID);
        
        // Create a name search pane that uses GBL.
        nameSearchPanel = new JPanel(new GridBagLayout());
        nameSearchPanel.setOpaque(false);
        
        // Create an arrray of names the user inputs.
        String[] names = nameField.getText().split(", ");
        if (names.length == 0 || !isArrayValid(names))
        {
            JOptionPane.showMessageDialog(this,
                    "Please enter a valid input. \n Example: " + name);
        } else
        {
            // If the nameSearchPanel is visible reset it.
            if (isNameSearchResultVisible)
            {
                searchPanel.remove(searchResultPanel);
                searchResultPanel = null;
                validate();
            }

            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.anchor = GridBagConstraints.CENTER;
            searchResultPanel = server.searchByName(names);
            searchPanel.add(searchResultPanel, gbc);
            // Add the searchResultPanel to the searchPanel.
            
            // Refresh the GUI.
            validate();
            
            // Set the panel value to visible.
            isNameSearchResultVisible = true;
        }
    }

    /**
     * Builds the hashtagSearchPanel that shows the users hashtag searches by
     * post.
     * @param hashtagField The hashtag the user is searching for.
     */
    private void buildHashtagSearchPanel(JTextField hashtagField)
    {
        // Create a CardLayout.
        CardLayout cL = (CardLayout) (cards.getLayout());
        cL.show(cards, HASHTAG_SEARCH_ID);
        
        // Create a JPanel that uses GBL.
        hashtagSearchPanel = new JPanel(new GridBagLayout());
        hashtagSearchPanel.setOpaque(false);
        
        // Checks for hashtags in all of the posts.
        String hashtag = hashtagField.getText();
        if (hashtag.length() != 0)
        {
            if (hashtag.charAt(0) != '#' || hashtag.equals(""))
            {
                JOptionPane.showMessageDialog(this,
                        "Please enter a valid hashtag. "
                        + "\n Example: #helloworld");
            } else
            {
                if (isHashtagSearchResultVisible)
                {
                    searchPanel.remove(searchResultPanel);
                    searchResultPanel = null;
                    validate();
                }

                gbc.gridx = 0;
                gbc.gridy = 3;
                gbc.anchor = GridBagConstraints.CENTER;
                searchResultPanel = server.searchByHashtag(hashtag);
                searchPanel.add(searchResultPanel, gbc);
                validate();
                isHashtagSearchResultVisible = true;
            }
        } else
        {
            JOptionPane.showMessageDialog(this,
                    "Please enter a valid hashtag. "
                    + "\n Example: #helloworld");
        }
    }

    /**
     * This builds a JPanel that shows all of the posts that have the user
     * specified sub string that is searched for.
     * 
     * @param substringField The sub string the user is searching for in the 
     * posts.
     */
    private void buildSubstringSearchPanel(JTextField substringField)
    {
        // Creates a CardLayout.
        CardLayout cL = (CardLayout) (cards.getLayout());
        cL.show(cards, SUBSTRING_SEARCH_ID);
        
        // Creates a JPanel that uses GBL.
        substringSearchPanel = new JPanel(new GridBagLayout());
        substringSearchPanel.setOpaque(false);
        
        String substring = substringField.getText();

        if (substring.equals(""))
        {
            JOptionPane.showMessageDialog(this,
                    "Please enter a valid input.");
        } else
        {
            if (isSubstringSearchResultVisible)
            {
                searchPanel.remove(searchResultPanel);
                searchResultPanel = null;
                validate();
            }

            searchResultPanel = server.searchBySubstring(substring);
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.anchor = GridBagConstraints.CENTER;
            searchPanel.add(searchResultPanel, gbc);
            validate();
            isSubstringSearchResultVisible = true;
        }
    }

    /**
     * This method uses a linear search to make sure each index of a String
     * array is filled with some text.
     *
     * @param array The array to loop over and check.
     * @return True if the array is valid, false otherwise.
     */
    private boolean isArrayValid(String[] array)
    {
        int count = 0;

        for (String element : array)
            if (!element.equals(""))
                count++;
        
        return (count == array.length);
    }
}
