package mackbook;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * /
 **
 * <pre>
 * Author(s): Jared Rathbun and Alexander Royer
 * Course: CSC 2620
 * Due Date: 10-9-20
 *
 * This class serves as a central part to the application, storing and managing
 * the timeline. It also allows for searching and saving.
 * </pre>
 */
public class Server extends Thread
{
    private int port;
    private Socket windowSocket = null;
    private ServerSocket serverSocket = null;
    public ArrayList<WindowThread> windowList;
    private ArrayList<Post> timeline;
    private String[] names;

    /**
     * Constructor that initializes the ServerSocket object.
     *
     * @param port The port the server is on.
     * @param names The array of the user's names.
     */
    public Server(int port, String[] names)
    {
        this.port = port;
        this.names = names;
        windowList = new ArrayList<>();
        timeline = new ArrayList<>();

        // Try to createa a server socket.
        try
        {
            serverSocket = new ServerSocket(this.port);
        } catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Error Creating Server Socket "
                    + "\n Reason: IOException");
        }
    }

    /**
     * Server's run method that spawns a new thread whenever one of the client
     * windows connects.
     */
    @Override
    public void run()
    {
        // Run an infinite loop that searches for a new connection.
        while (true)
        {
            try
            {
                windowSocket = serverSocket.accept();
                WindowThread windowThread = new WindowThread(windowSocket);
                windowThread.start();
                windowList.add(windowThread);
            } catch (IOException e)
            {
                System.err.println("Connection to client can't be "
                        + "established.");
            }
        }
    }

    /**
     * This method posts a post object to all of the windows.
     *
     * @param post The object to send to all windows.
     */
    public void postToAllWindows(Post post)
    {
        windowList.get(0).post(post);
    }

    /**
     * This method saves the current timeline that is either made or loaded.
     */
    public void saveTimeline()
    {
        // Create tje JFileChooser object.
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileNameExtensionFilter("Mackbook File (.mb)",
                "mb"));

        // Create the File object.
        File file = null;
        if (jfc.showSaveDialog(jfc) == JFileChooser.APPROVE_OPTION)
        {
            file = new File(jfc.getSelectedFile() + ".mb");
        }

        if (file != null)
        {
            try
            {
                // Create the PrintStream object.
                PrintStream output = new PrintStream(file);

                // Print each name seperated by commas.
                for (int i = 0; i < names.length; i++)
                {
                    if (i == (names.length - 1))
                    {
                        output.print(names[i]);
                    } else
                    {
                        output.print(names[i] + ", ");
                    }
                }

                // Print a blank line.
                output.println("\n");

                /* Get the details for every post, and print them on their
                own line. */
                timeline.forEach((post) ->
                {
                    String posterName = post.getName();
                    String text = post.getText();
                    String URL = post.getURL();
                    String imgPath = post.getImgPath();
                    String date = post.getDate();
                    output.println(posterName + ", " + text + ", " + URL + ", "
                            + imgPath + ", " + date);
                });

            } catch (FileNotFoundException ex)
            {
                JOptionPane.showMessageDialog(null, "Error saving timeline.");
                return;
            }

            // Give feedback to the user the timeline was saved succesfully.
            JOptionPane.showMessageDialog(null, "Succesfully save timeline as "
                    + file.getName());
        }
    }

    /**
     * This method adds a new post to the timeline.
     *
     * @param newPost The post the client is adding to the timeline.
     */
    public void addNewPost(Post newPost)
    {
        timeline.add(newPost);
    }

    /**
     * This method gets the current timeline (ArrayList) of posts.
     *
     * @return An array of Posts.
     */
    public Post[] getTimeline()
    {
        return timeline.toArray(new Post[timeline.size()]);
    }

    /**
     * Returns a JScrollPane containing a list of the Posts that match the
     * specified hashtag.
     *
     * @param hashtag The hashtag to search for.
     * @return A JScrollPane containing the posts that match.
     */
    public JScrollPane searchByHashtag(String hashtag)
    {
        // Precondition check.
        assert (hashtag.charAt(0) == '#');

        // A JPanel for the result.
        JPanel resultPanel = new JPanel();

        // A counter for the number of posts that match the search.
        int count = 0;

        // A counter for the height of the resultPanel.
        int height = 0;

        for (Post post : timeline)
        {
            String text = post.getText();

            Pattern pattern = Pattern.compile("(\\s*\\w\\d*)*" + hashtag
                    + "(\\s*\\w\\d*)*");

            Matcher matcher = pattern.matcher(text);

            if (matcher.find())
            {
                height += post.getHeight();
                resultPanel.setPreferredSize(new Dimension(670, height));
                resultPanel.add(post);
                resultPanel.validate();
                count++;
            }
        }
        // Return the JSrollPane if the count > 0, otherwise return empty panel.
        if (count > 0)
        {
            JScrollPane resultPane = new JScrollPane(resultPanel);
            resultPane.setPreferredSize(new Dimension(540, 250));
            return resultPane;
        } else
        {
            JPanel emptyPanel = new JPanel();
            emptyPanel.add(new JLabel("The search did not find any matching "
                    + "posts."));

            return new JScrollPane(emptyPanel);
        }
    }

    /**
     * Searches the timeline for any posts made by a specific name or set of
     * names.
     *
     * @param names The name(s) of users posts to search for.
     * @return A JScrollPane of posts that are made by the specific name(s).
     */
    public JScrollPane searchByName(String[] names)
    {
        // Precondition check.
        for (String name : names)
        // Assert the name is not null or empty.
        {
            assert (name != null) && (!name.equals(""));
        }

        // A new JPanel for the output of the search.
        JPanel resultPanel = new JPanel();

        // A counter for the number of objects found that match the search.
        int count = 0;
        // A counter for the height of the resultPanel.
        int height = 0;

        // Loop over every names in the array of names.
        for (String name : names)
        {
            Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);

            // Loop over every Post in hte timeline and compare the names.
            for (Post post : timeline)
            {
                // Create a matcher object.
                Matcher matcher = pattern.matcher(post.getName());

                // If the name and the post's name, add it to the resultPanel.
                if (matcher.find())
                {
                    height += post.getHeight();
                    resultPanel.setPreferredSize(new Dimension(670, height));
                    resultPanel.add(post);
                    resultPanel.validate();
                    count++;
                }
            }
        }
        // Return the JSrollPane if the count > 0, otherwise return empty panel.
        if (count > 0)
        {
            JScrollPane resultPane = new JScrollPane(resultPanel);
            resultPane.setPreferredSize(new Dimension(540, 250));
            return resultPane;
        } else
        {
            // Create an empty JLabel.
            JPanel emptyPanel = new JPanel();
            emptyPanel.add(new JLabel("The search did not find any matching "
                    + "posts."));

            // Return the scroll pane with the JPanel.
            return new JScrollPane(emptyPanel);
        }
    }

    /**
     * This method creates a JScrollPane that searches the posts by a user
     * specified sub string.
     *
     * @param substring A user specified sub string.
     * @return A JScrollPane that has all of the posts that contain the sub
     * string.
     */
    public JScrollPane searchBySubstring(String substring)
    {
        // Assert the substring is not blank.
        assert (!substring.equals(""));

        // A JPanel for the result.
        JPanel resultPanel = new JPanel();

        // A counter for the number of posts that match the search.
        int count = 0;
        
        // A counter for the height of the resultPanel.
        int height = 0;
        // Loop through every post in the timeline.
        for (Post post : timeline)
        {
            String text = post.getText();

            // Create the Patter object.
            Pattern pattern = Pattern.compile("(\\s*\\w\\d*)" + substring
                    + "(\\s*\\w\\d*)");

            // Create the Matcher object.
            Matcher matcher = pattern.matcher(text);

            /* If the matcher finds the sub string in a post, add it to the 
            panel. */
            if (matcher.find())
            {
                height += post.getHeight();
                resultPanel.setPreferredSize(new Dimension(670, height));
                resultPanel.add(post);
                resultPanel.validate();
                count++;
            }
        }

        // Return the JSrollPane if the count > 0, otherwise return empty panel.
        if (count > 0)
        {
            JScrollPane resultPane = new JScrollPane(resultPanel);
            resultPane.setPreferredSize(new Dimension(540, 250));
            return resultPane;
        } else
        {
            JPanel emptyPanel = new JPanel();
            emptyPanel.add(new JLabel("The search did not find any matching "
                    + "posts."));

            return new JScrollPane(emptyPanel);
        }
    }

    public class WindowThread extends Thread
    {

        // Create I/O Streams.
        private ObjectInputStream inputStream = null;
        private ObjectOutputStream outputStream = null;

        /**
         * Creates a WindowThread object that passes in a Socket and connect to
         * a server.
         *
         * @param connection
         */
        public WindowThread(Socket connection)
        {
            try
            {
                // Try and connect to server.
                inputStream = new ObjectInputStream(connection
                        .getInputStream());
                outputStream = new ObjectOutputStream(connection
                        .getOutputStream());
            } catch (IOException e)
            {
                System.err.println("Error with I/O at server.");
            }
        }

        /**
         * This method reads posts from the client continuously. Once a post is
         * received it adds it to window.
         */
        @Override
        public void run()
        {
            /* Reads message from client continuously, and when one is received
            it adds the new text to the "chat window" string that controls
            the entire chat window for each chat client. */
            while (true)
            {
                // Creates post object for any read posts.
                Post readPost = null;

                try
                {
                    readPost = (Post) inputStream.readObject();
                } catch (IOException | ClassNotFoundException ex)
                {
                    System.err.println(ex.getMessage());
                }

                // Send the Post to all active windows.
                postToAllWindows(readPost);
            }
        }

        /**
         * This method posts the newly made post object to the output stream.
         *
         * @param post The Post the user or client is posting to the timeline.
         */
        public void post(Post post)
        {
            /* Write the object to the output stream and then clear the output
            stream. */
            try
            {
                outputStream.writeObject(post);
                outputStream.flush();
            } catch (IOException ex)
            {
                // Print an error message if the post doesn't post to server.
                System.err.println("Error Writing Post to Server.");
            }
        }
    }
}
