package mackbook;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <pre>
 * Authors: Jared Rathbun and Alexander Royer
 * Course: CSC 2620
 * Due Date: 10-9-20
 *
 * Serves as the entry point to the program, prompts the user for the choice to
 * either load an existing timeline file or create a new timeline by specifying 
 * the number of users.
 * 
 * COLORS REFERENCED FROM: 
 * https://www.merrimack.edu/styles/styleguide.php
 * 
 * </pre>
 */
public class Project5 extends JDialog
{
    private final String LOCAL_HOST = "127.0.0.1";
    private final int PORT = 5000;
    private static String[] names;
    private static GridBagConstraints gbc;
    private File file;
    private boolean isCreatePanelVisible, isFieldsPanelVisible;
    private JPanel createPanel, fieldsPanel;
    private Server server;
    
    /**
     * Constructor that takes the parent frame as a parameter. Prompts the user
     * for a choice using JDialog.
     * 
     * @param parent The parent frame. (null is used here)
     */
    public Project5(Frame parent)
    {
        // Super call to JDialog.
        super(parent); 
        
        pack();
        setTitle("Welcome to MackBook");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(400, 400);
        setVisible(true);
        
        // Set a Gradient background using Merrimack's colors.
        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;
                final int width = getWidth();
                final int height = getHeight();
                g2d.setPaint(new GradientPaint(0, 0, new Color(94, 173, 203),
                        width, height, new Color(6, 52, 91), true));
                g2d.fillRect(0, 0, width, height);
            }
        };
        setContentPane(panel);
        
        // Create a GBC and set the layout.
        gbc = new GridBagConstraints();
        setLayout(new GridBagLayout());
        promptUser();
    }
    
    /**
     * Entry point to the program. Constructs new Project5 object.
     * 
     * @param args The command line arguments. 
     */
    public static void main(String[] args)
    {
        new Project5(null);
    }
    
    /**
     * This method prompts the user for the choice to either create a new 
     * timeline or load an existing timeline.
     */
    private void promptUser()
    {
        // Set the IconImage of the JDialog.
        ImageIcon icon = new ImageIcon("mprofilePictureTransparent.png");
        setIconImage(icon.getImage());
        
        // Create a header and add it to the frame.
        JPanel headerPanel = new JPanel();
        headerPanel.setOpaque(false);
        JLabel headerLabel = new JLabel("<HTML><U>Welcome to MackBook! "
                + "(Not affiliated with Facebook&reg;)</U></HTML>");
        headerLabel.setForeground(Color.WHITE);
        headerPanel.add(headerLabel);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;  
        add(headerPanel, gbc);
        
        // Create a new JPanel for the options buttons.
        JPanel optionsPanel = new JPanel(new GridBagLayout());
        optionsPanel.setOpaque(false);
        optionsPanel.setForeground(Color.WHITE);
        optionsPanel.setBorder(BorderFactory.createTitledBorder(
                "Select an Option"));
        
        // Set the title of the optionsPanel's color to white.
        TitledBorder opBorder = (TitledBorder) optionsPanel.getBorder();
        opBorder.setTitleColor(Color.WHITE);
        
        // A JButton to create a new timeline.
        JButton createButton = new JButton("Create a new Timeline");
        createButton.addActionListener((ActionEvent e) -> {
            initCreatePanel();
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        optionsPanel.add(createButton, gbc);
        // Add the createButton to the optionsPanel.
        
        // A JButton to load an existing timeline (.txt) file.
        JButton loadButton = new JButton("Load an Existing Timeline");
        loadButton.addActionListener((ActionEvent e) -> {
            readTimelineFile();
        });
        
        // Add the loadButton.
        gbc.insets = new Insets(10,10,10,10);
        gbc.gridx = 1;
        gbc.gridy = 0;
        optionsPanel.add(loadButton, gbc);
        
        // Add the optionsPanel to the GUI.
        gbc.insets = new Insets(0,0,0,0);
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(optionsPanel, gbc);
        
        // Refresh GUI.
        validate();
    }
    
    /**
     * This method initializes the panel that allows the user to create a new 
     * set of members for the timeline.
     */
    private void initCreatePanel()
    {
        // An array for the choices of the JComboBox.
        final Integer[] numbersArray = { 3, 4, 5, 6, 7 };
        
        // If the panel is already loaded, remove and revalidate.
        if (isCreatePanelVisible)
        {
            remove(createPanel);
            createPanel = null;
            // Refresh GUI.
            validate();
        }
        
        /* Create a new panel for the creation of the timeline, using a 
        GridBagLayout and a Border. */
        createPanel = new JPanel(new GridBagLayout());
        createPanel.setPreferredSize(new Dimension(180, 80));
        createPanel.setOpaque(false);
        createPanel.setBorder(BorderFactory.createTitledBorder("Please fill "
                + "in the following"));
        
        // Set the title of the border to be white.
        TitledBorder titledBorder = (TitledBorder) createPanel.getBorder();
        titledBorder.setTitleColor(Color.WHITE);
        
        // Create and add a new JLabel.
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        JLabel usersLabel = new JLabel("How Many Users?  ");
        usersLabel.setForeground(Color.WHITE);
        createPanel.add(usersLabel, gbc);
        
        int[] numOfUsers = new int[1];
        // Create and add a new JComboBox for the number of boxes.
        JComboBox usersBox = new JComboBox(numbersArray);
        usersBox.setSelectedIndex(0);
        
        // Initializes an ArrayList of JTextFields.
        ArrayList<JTextField> textFields = new ArrayList<>();
        boolean[] isArrayListFilled = { false };
        usersBox.addActionListener((ActionEvent e) -> {
            if (isArrayListFilled[0])
                textFields.clear();
                
            // Get the number of users to create.
            numOfUsers[0] = Integer.parseInt(String.valueOf(usersBox
                .getSelectedItem()));
            
            validate();
            
            /* Create a set of fields that allows the user to enter everyone's 
            names. */
            if (isFieldsPanelVisible)
            {
                remove(fieldsPanel);
                fieldsPanel = null;
                names = null;
                // Refresh the GUI.
                validate();
            }
            
            /* Create a new panel for the fields. Populate the correct number of 
            JTextFields by using the number the user selects. */
            fieldsPanel = new JPanel(new GridBagLayout());
            fieldsPanel.setOpaque(false);
            for (int i = 0; i < numOfUsers[0]; i++)
                textFields.add(new JTextField(15));
            
            int x = 0;
            int y = 0;
            for (int i = 0; i < numOfUsers[0]; i++)
            {
                gbc.gridx = x;
                gbc.gridy = y;
                gbc.anchor = GridBagConstraints.LINE_END;
                JLabel nameLabel = new JLabel("User " + (i + 1) + "'s Name: ");
                nameLabel.setForeground(Color.WHITE);
                fieldsPanel.add(nameLabel, gbc);
                gbc.gridx = x + 1;
                gbc.gridy = y;
                gbc.anchor = GridBagConstraints.LINE_START;
                fieldsPanel.add(textFields.get(i), gbc); 
                y++;
            }
            
            // Add the number of users to the GUI.
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.insets = new Insets(5,5,5,5);
            gbc.anchor = GridBagConstraints.CENTER;
            add(fieldsPanel, gbc);
            pack();
            validate();
            isFieldsPanelVisible = true;
            isArrayListFilled[0] = true;
        });
        
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        createPanel.add(usersBox, gbc);
        // Add the usersBox to the createPanel.
        
        // A panel for the buttons.
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setOpaque(false);
        JButton okButton = new JButton("OK");
        okButton.addActionListener((ActionEvent e) -> {  
            int count = 0;
            
            count = textFields.stream().filter((jtf) -> (!jtf.getText().equals
        (""))).map((_item) -> 1).reduce(count, Integer::sum);
            boolean isInputValid = (count == textFields.size());
            
            if (isInputValid)
            {
                names = new String[count];
                for (int i = 0; i < names.length; i++)
                    names[i] = textFields.get(i).getText();
                dispose();
                createWindows();
            }
            else
            {
                JOptionPane.showMessageDialog(this, 
                        "Please fill in all fields.");
            }
        });
        buttonsPanel.add(okButton);
        // Add the okButton to the buttonsPanel.
        
        // Creates the cancel button.
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((ActionEvent e) -> {
           System.exit(0); 
        });
        buttonsPanel.add(cancelButton);
        // Add the cancel button to the buttonsPanel.
        
        // Add the buttons panel to the GUI.
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        add(buttonsPanel, gbc);
        
        // Add the createPanel itself to the GUI.
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        add(createPanel, gbc);
        validate();
        isCreatePanelVisible = true;
    }
    
    /**
     * This method creates the windows used to display the timeline. It uses 
     * the name field as a tracker for how many windows to create.
     */
    private void createWindows()
    {
        // Start the server.
        server = new Server(PORT, names);
        server.start();
        
        // Loop over every name, create a new Window, and start it.
        for (String name : names)
        {
            Window window = new Window(name, LOCAL_HOST, PORT, server);
            Thread thread = new Thread(window);
            thread.start();
        }
    }

    /**
     * This method reads a timeline file that the user selects.
     */
    private void readTimelineFile()
    {
        // Initialize file reading objects.
        JFileChooser jfc = new JFileChooser();
        BufferedReader br = null;
        ArrayList<Post> postList = new ArrayList<>();
        
        // Initailize the File Chooser.
        jfc.setDialogTitle("Please Select A Timeline.");
        jfc.setFileFilter(new FileNameExtensionFilter("Mackbook File (.mb)", 
                "mb"));
        if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
            file = jfc.getSelectedFile();
        
        if (file != null)
        {
            try 
            {
                // Initialize the BufferedReader.
                br = new BufferedReader(new FileReader(file));
                
                // Capture the names and create a new window from each name.
                String[] windowNames = br.readLine().split(", ");
                names = windowNames;
                
                // Dump the blank line.
                br.readLine();
                
                // An array for each line of the file.
                String[] lineArray;
                
                // A String object for each line of the file.
                String line; 
                
                // Identifiers for the array indicies.
                final int NAME = 0;
                final int TEXT = 1;
                final int URL = 2;
                final int FILE_PATH = 3;
                final int DATE = 4;
                
                // Loop over the remaining lines of text.
                while ((line = br.readLine()) != null)
                {
                    Post newPost = null;
                    
                    lineArray = line.split(", ");
                    
                    // Check to make sure the file has not been edited.
                    assert (lineArray.length == 5);
                    
                    /* Use a chain of if-else statements to find which type of 
                    Post to create. */
                    if (lineArray[URL].equals("url") && 
                            lineArray[FILE_PATH].equals("imgpath") && 
                            lineArray[URL].equals("url"))
                        newPost = new Post(lineArray[NAME], lineArray[TEXT]);
                    else if (lineArray[TEXT].equals("text") && 
                            lineArray[FILE_PATH].equals("imgpath"))
                        newPost = new Post(lineArray[TEXT], lineArray[URL], 
                                false);
                    else if (lineArray[TEXT].equals("text") && 
                            lineArray[URL].equals("url"))
                        newPost = new Post(lineArray[NAME], 
                                new File(lineArray[FILE_PATH]));
                    else if (lineArray[URL].equals("url"))
                        newPost = new Post(lineArray[NAME], lineArray[TEXT], 
                                new File(lineArray[FILE_PATH]));
                    else if (lineArray[FILE_PATH].equals("imgpath"))
                        newPost = new Post(lineArray[NAME], lineArray[TEXT], 
                                lineArray[URL]);
                    else
                        newPost = new Post(lineArray[NAME], lineArray[TEXT], 
                            lineArray[URL], new File(lineArray[FILE_PATH]));

                    // Set the date of the post.
                    newPost.setDate(lineArray[DATE]);
                    
                    // Add the post to the ArrayList.
                    postList.add(newPost);
                }
                
            } catch (IOException e)
            {
                // Return and error message.
                JOptionPane.showMessageDialog(null, "Unable to read file.");
                return;
            }
            
            // Create the windows.
            createWindows();
            
            // Add the posts to the server. 
            for(int i = 0; i < postList.size(); i++)
            {
                server.addNewPost(postList.get(i));
            }
        }
    }
}
