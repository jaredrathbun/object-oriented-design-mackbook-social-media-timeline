package mackbook;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.Date;
import javax.imageio.ImageIO;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * <pre>
 * Author(s): Jared Rathbun and Alexander Royer
 * Course: CSC2620
 * Due Date: November 9th, 2020
 *
 * This class servers as a holder for Post. It inherits from JPanel which allows
 * for the post to be displayed directly on it.
 * </pre>
 */
public class Post extends JPanel implements Serializable
{
    private String name;
    private String text;
    private URL url;
    transient private BufferedImage img = null;
    transient private BufferedImage profileImg;
    private String imgPath;
    private static GridBagConstraints gbc;
    private Date date;
    
    /**
     * This constructor is used to create a Post object that has a name, a text 
     * message, a URL, and an image that the user is trying to upload.
     * @param name The users name.
     * @param text The text message the user is trying to upload.
     * @param url The URL the user is trying to upload.
     * @param img The image the user is trying to upload.
     * @throws java.net.MalformedURLException
     */
    public Post(String name, String text, String url, File img) 
            throws MalformedURLException
    {
        this.name = name;
        this.text = text;
        createURL(url);
        createImage(img);
        createProfileImg();
        drawPanel();
    }

    /**
     * This constructor is used to create a Post object that has a name, a text
     * message, and a URL that the user is trying to upload.
     * @param name The users name.
     * @param text The text message the user is trying to upload.
     * @param url The URL the user is trying to upload.
     * @throws java.net.MalformedURLException
     */
    public Post(String name, String text, String url) 
            throws MalformedURLException
    {
        this.name = name;
        this.text = text;
        this.url = new URL(url);
        this.img = null;
        createProfileImg();
        drawPanel();
    }

    /**
     * This constructor is used to create a Post object that has a name, a text
     * message, and an image that the user is trying to upload.
     * @param name The users name.
     * @param text The text message the user is trying to upload.
     * @param img The image the user is trying to upload.
     */
    public Post(String name, String text, File img)
    {
        this.name = name;
        this.text = text;
        createImage(img);
        createProfileImg();
        drawPanel();
    }

    /**
     * This constructor is used to create a Post object that has a name and the
     * text message that the user is trying to upload.
     * @param name The users name.
     * @param text The text message the user is trying to upload.
     */
    public Post(String name, String text)
    {
        this.name = name;
        this.text = text;
        this.img = null;
        createProfileImg();
        drawPanel();
    }

    /**
     * This constructor is used to create a Post object that has a name and
     * URL in it that the user is trying to upload.
     * 
     * @param name The users name.
     * @param url The URL the user is trying to upload.
     * @param flag A useless boolean to differentiate between constructors.
     * 
     * @throws java.net.MalformedURLException
     */
    public Post(String name, String url, boolean flag) 
            throws MalformedURLException
    {
        this.name = name;
        this.img = null;
        createURL(url);
        createProfileImg();
        drawPanel();
    }

    /**
     * This constructor is used to create a Post object that has a name and 
     * image in it that the user is trying to upload.
     * @param name The users name.
     * @param img The image the user is trying to upload.
     */
    public Post(String name, File img)
    {
        this.name = name;
        createImage(img);
        createProfileImg();
        drawPanel();
    }

    /**
     * Source: https://www.java2novice.com/java_networking/create_URL_object/
     * 
     * This method creates a URL object from a String.
     * @param URL a string that is a valid URL.
     */
    private void createURL(String URL) throws MalformedURLException
    {
        URL formattedURL = new URL(URL);
        url = formattedURL;
    }
    
    /**
     * Source: https://www.java2novice.com/java_networking/create_URL_object/
     * 
     * This method creates an Image with a File.
     * @param file an Image of type File.
     */
    private void createImage(File file)
    {
        //// Changed the parameted name from img to file. ////
        try
        {
            img = ImageIO.read(file);
            imgPath = file.getPath();
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    /**
     * This method loads the profile image for each post into the profileImg
     * field.
     */
    private void createProfileImg()
    {
        try
        {
            profileImg = ImageIO.read(new File("mProfilePicture.png"));
        } catch (IOException ex)
        {
            // Swallow exception.
        }
    }
    
    /**
     * This method draws the post on the JPanel.
     */
    private void drawPanel()
    {
        // Set the size of the panel.
        setSize();
        
        // Set the background color of the panel.
        setBackground(new Color(137, 150, 150));
        
        // Set the Date to the current system's time if it hasn't been set.
        if (date == null)
            date = new Date();
        
        // Allocate the gbc object.
        gbc = new GridBagConstraints();
        
        // GUI stuff.
        setVisible(true);
        setLayout(new GridBagLayout());
        setOpaque(false);
        
        // Create a panel for the profile. Add the profileImg and name/date. 
        JPanel profilePanel = new JPanel(new GridBagLayout());
        profilePanel.setOpaque(false);
        JLabel profileImgLabel = new JLabel(new ImageIcon(profileImg));
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.anchor = GridBagConstraints.LINE_START;
        profilePanel.add(profileImgLabel);
        
        // Create a panel for the name and date, then add the name.
        JPanel nameAndDatePanel = new JPanel(new GridBagLayout());
        JLabel nameLabel = new JLabel(name);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weighty = .5;
        gbc.insets = new Insets(0, 5, 0, 0);
        gbc.anchor = GridBagConstraints.LINE_START;
        nameAndDatePanel.add(nameLabel, gbc);
        
        // Add A JLabel for the date.
        JLabel dateLabel = new JLabel(date.toString());
        dateLabel.setForeground(Color.GRAY);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weighty = .5;
        gbc.anchor = GridBagConstraints.LINE_START;
        nameAndDatePanel.add(dateLabel, gbc);
        profilePanel.add(nameAndDatePanel);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        add(profilePanel, gbc);
        
        /*Create a new JTextArea for the text and set it to non-editable and 
        set line wraps to true. */
        JTextArea textArea = new JTextArea(40, 40);
        //textArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        textArea.setLineWrap(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        textArea.setSize(500, 100);
        
        // If the text is allocated, append it to the text box.
        if (text != null)
            textArea.append(text);
        
        // If the URL is allocated, set the color to blue and append to box.
        if (url != null)
        {
            textArea.setForeground(Color.BLUE);
            textArea.append("\n\nURL: " + url.toExternalForm());
            textArea.setForeground(Color.BLACK);
        }
        
        // If the textArea has text, add it to the panel.
        if (!textArea.getText().equals(""))
        {
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weighty = .1;
            gbc.weightx = .1;
            add(textArea, gbc);
        }
        
        /* If the img is allocated, resize it to fit in the panel and add it 
        as a JLabel. */
        if (img != null)
        {
            Image resizeImg = new BufferedImage(400, 450, Image.SCALE_SMOOTH);
            Image result = img.getScaledInstance(400, 450, 
                    BufferedImage.TYPE_INT_RGB);
            resizeImg.getGraphics().drawImage(result, 0, 0, 400, 450, null);
            
            JLabel picLabel = new JLabel(new ImageIcon(resizeImg));
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.anchor = GridBagConstraints.LINE_START;
            add(picLabel, gbc);
        }
    }
    
    /**
     * paintComponent method that overrides from JPanel. Draw a stroked 
     * horizontal line at the top of the GUI.
     * 
     * @param g The Graphics object.
     */
    @Override
    public void paintComponent(Graphics g)
    {
        // Draw a black horizontal line at the top of the panel.
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(5.0f));
        g2d.drawLine(0, 0, (int) getSize().getWidth(), 0);
    }
    
    /**
     * This method sets the size of the JPanel, depending on if an image was 
     * entered or not.
     */
    private void setSize()
    {
        if (img != null)
            setPreferredSize(new Dimension(670, img.getHeight() + 50));
        else 
            setPreferredSize(new Dimension(670, 125));
    }
    
    /**
     * toString method that overrides from Object.
     * 
     * @return The name of the poster. 
     */
    @Override
    public String getName()
    {
        return name;
    }
    
    /**
     * Getter for the text of the post.
     * 
     * @return The text entered into the post. 
     */
    public String getText()
    {
        if (text != null)
            return text;
        else
            return "text";
    }
    
    /**
     * Getter for a String representation of the URL.
     * 
     * @return A string containing the URL.
     */
    public String getURL()
    {
        if (url != null)
            return url.toString();
        else
            return "url";
    }
    
    /**
     * Getter for the path of the image.
     * 
     * @return The path of the image as a String. 
     */
    public String getImgPath()
    {
        if (imgPath != null)
            return imgPath;
        else
            return "imgpath";
    }
    
    /**
     * Getter for the Date as String.
     * 
     * @return A String representation of the Date. 
     */
    public String getDate()
    {
        if (date != null)
            return date.toString();
        else 
            return "date";
    }
    
    /**
     * This method returns whether or not the post has an image.
     * 
     * @return True if the image is null, false otherwise. 
     */
    public boolean hasImage()
    {
        return (img == null);
    }
    
    /**
     * Setter for the date. Useful when loading the timeline in.
     * 
     * @param dateString A string representation of the date.
     */
    public void setDate(String dateString)
    {
       date = new Date(dateString);
    }
}
